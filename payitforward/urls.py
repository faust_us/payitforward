from django.conf.urls import patterns, include, url
from django.contrib import admin
from tasks.views import *

urlpatterns = patterns('',
    # Examples:
    # url(r'^$', 'payitforward.views.home', name='home'),
    # url(r'^blog/', include('blog.urls')),

    url(r'^task/([0-9]+)?/$', task),
    url(r'^tasks/$', tasks),
    url(r'^create_task/$', create_task),
    url(r'^profile/([0-9]+)?/$', profile),
    url(r'^people/$', people),
    url(r'^auth/', include('loginsys.urls')),
    url(r'^$', index),


    url(r'^admin/', include(admin.site.urls)),

    #develop
    url(r'^uploads/(?P<path>.*)$', 'django.views.static.serve',
        {'document_root': '/home/alex/djcode/payitforward/uploads'}),
    url(r'^media/(?P<path>.*)$', 'django.views.static.serve',
        {'document_root': '/home/alex/djcode/payitforward/media'}),
)

# -*- coding: utf-8 -*-
from django.shortcuts import render_to_response, redirect
from django.contrib import auth, messages
from django.core.context_processors import csrf


def login(request):
    response = {}
    response.update(csrf(request))
    if request.POST:
        username = request.POST.get('username', '')
        password = request.POST.get('password', '')
        user = auth.authenticate(username=username, password=password)
        if user is not None:
            auth.login(request, user)
            message = "вход выполнен успешно"
            messages.add_message(request, messages.INFO, message)
            return redirect('/')
        else:
            response['login_error'] = "Пользователь не найден"
            message = "Пользователь не найден"
            messages.add_message(request, messages.INFO, message)
            return redirect('/')
            #return render_to_response('login.html', response)
    else:
        #return render_to_response('login.html', response)
        return redirect('/')


def logout(request):
    auth.logout(request)
    return redirect('/')

from django.conf.urls import patterns, include, url
from loginsys.views import *

urlpatterns = patterns('',

    url(r'^login/$', login),
    url(r'^logout/$', logout),

)
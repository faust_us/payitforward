# -*- coding: utf-8 -*-
from django.db import models
from django.contrib.auth.models import User


class Category(models.Model):
    name = models.CharField(max_length=50, verbose_name='Категория транслитом')
    title = models.CharField(max_length=50, verbose_name='Категория')

    class Meta:
        verbose_name = 'категории'
        verbose_name_plural = 'категория'

    def __unicode__(self):
        return self.title


class Tag(models.Model):
    name = models.CharField(max_length=50, verbose_name='Тег транслитом')
    title = models.CharField(max_length=50, verbose_name='Тег')

    class Meta:
        verbose_name = 'теги'
        verbose_name_plural = 'тег'

    def __unicode__(self):
        return self.title


class Status(models.Model):
    name = models.CharField(verbose_name='Статус', max_length=30)

    class Meta:
        verbose_name = 'статусы'
        verbose_name_plural = 'статус'

    def __unicode__(self):
        return self.name


class Task(models.Model):
    user = models.ForeignKey(User)
    title = models.CharField(max_length=100, verbose_name='Название задачи')
    description = models.TextField(verbose_name='Описание')
    category = models.ForeignKey(Category, related_name='tasks', verbose_name='Категория')
    tag = models.ManyToManyField(Tag, related_name='tasks', verbose_name='Тег')
    #end_date = models.DateTimeField(blank=True, verbose_name='Сроки задачи')
    create_date = models.DateTimeField(auto_now_add=True, verbose_name='Дата создания')
    rate = models.IntegerField(default=0, verbose_name='Рейтинг')
    task_photo = models.ImageField(upload_to='task_photos', verbose_name='Фото', blank=True, default='task_photos/default.jpg')
    status = models.ForeignKey(Status, default=1)

    class Meta:
        verbose_name = 'задачи'
        verbose_name_plural = 'задача'

    def __unicode__(self):
        return self.title


class Answer(models.Model):
    content = models.TextField()
    user = models.ForeignKey(User)
    task = models.ForeignKey(Task, related_name='answers')
    create_date = models.DateTimeField(auto_now_add=True, verbose_name='Дата создания')

    class Meta:
        verbose_name = 'отклики'
        verbose_name_plural = 'отклик'

    def __unicode__(self):
        return self.content


class Profile(models.Model):
    user = models.ForeignKey(User, related_name='profile')
    ava = models.ImageField(upload_to='avatars')
    karma = models.IntegerField(default=0)
    end_tasks = models.IntegerField(default=0)
    gevt_tasks = models.IntegerField(default=0)

    class Meta:
        verbose_name = 'профили пользователей'
        verbose_name_plural = 'Профиль пользователя'

    def __unicode__(self):
        return self.user.get_full_name()
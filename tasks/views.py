from django.shortcuts import render_to_response, redirect
from django.http import HttpResponseBadRequest
from django.template import RequestContext
from tasks.models import *
from forms import *
import random


def index(request):
    all_tasks = Task.objects.all()
    important_tasks = all_tasks.order_by('-rate')[0:4]
    fresh_tasks = all_tasks.order_by('-create_date')[0:4]
    task_form = CreateTaskForm()
    response = {'important_tasks': important_tasks, 'fresh_tasks': fresh_tasks, 'task_form': task_form}

    return render_to_response('index.html', response, context_instance=RequestContext(request))


def tasks(request):
    all_tasks = Task.objects.all()
    return render_to_response('tasks.html', {'tasks': all_tasks})


def task(request, task_id):
    one_task = Task.objects.get(id=task_id)
    exclude_tasks = Task.objects.exclude(id=task_id)[0:4]
    return render_to_response('task.html', {'task': one_task, 'tasks': exclude_tasks})


def create_task(request):
    if request.method == 'POST':
        form = CreateTaskForm(request.POST, request.FILES,)
        if form.is_valid():
            new_task = form.save(commit=False)
            if not new_task.task_photo:
                number = random.randint(1, 5)
                new_task.task_photo = 'task_photos/random_images/'+str(number)+'.jpg'
            new_task.user = request.user
            new_task.save()
            return redirect('/task/'+str(new_task.id))
    return HttpResponseBadRequest()


def people(request):
    all_profiles = Profile.objects.all()
    return render_to_response('people.html', {'profiles': all_profiles})


def profile(request, user_id):
    profile_info = Profile.objects.get(id=user_id)
    if profile_info is not None:
        return render_to_response('profile.html', {'profile': profile_info})
    else:
        return HttpResponseBadRequest()
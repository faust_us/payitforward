# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('tasks', '0002_task_rate'),
    ]

    operations = [
        migrations.AddField(
            model_name='task',
            name='task_photo',
            field=models.ImageField(default=b'task_photos/default.jpg', upload_to=b'task_photos', verbose_name=b'\xd0\xa4\xd0\xbe\xd1\x82\xd0\xbe', blank=True),
            preserve_default=True,
        ),
    ]

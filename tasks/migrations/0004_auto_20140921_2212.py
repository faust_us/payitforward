# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('tasks', '0003_task_task_photo'),
    ]

    operations = [
        migrations.CreateModel(
            name='Status',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('name', models.CharField(max_length=30, verbose_name=b'\xd0\xa1\xd1\x82\xd0\xb0\xd1\x82\xd1\x83\xd1\x81')),
            ],
            options={
                'verbose_name': '\u0441\u0442\u0430\u0442\u0443\u0441\u044b',
                'verbose_name_plural': '\u0441\u0442\u0430\u0442\u0443\u0441',
            },
            bases=(models.Model,),
        ),
        migrations.AddField(
            model_name='task',
            name='status',
            field=models.ForeignKey(default=1, to='tasks.Status'),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='task',
            name='category',
            field=models.ForeignKey(related_name=b'tasks', verbose_name=b'\xd0\x9a\xd0\xb0\xd1\x82\xd0\xb5\xd0\xb3\xd0\xbe\xd1\x80\xd0\xb8\xd1\x8f', to='tasks.Category'),
        ),
        migrations.AlterField(
            model_name='task',
            name='tag',
            field=models.ManyToManyField(related_name=b'tasks', verbose_name=b'\xd0\xa2\xd0\xb5\xd0\xb3', to=b'tasks.Tag'),
        ),
    ]
